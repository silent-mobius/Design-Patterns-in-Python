# Design-Patterns-in-Python
Most commonly used python design patterns

The patterns are found: design_patterns/venv/include/design_patterns
[Design patterns](https://github.com/NapsterZ4/Design-Patterns-in-Python/tree/master/design_patterns/venv/include/design_patterns)


**1. Abstract Factory: ** The abstract factory method provides an interface to create dependent related objects, in the example used, we have a pet store that has dogs and cats and depending on the factory we choose (in this case we use the python library "random" so that it automatically decides the factory with which it will work) we will call that factory but it is possible to do it because they respect the same interface. In the pet store class we have the output function that shows us if it is a dog or a cat with the characteristic sound of the animal

[Abstract Factory pattern code](https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/abstract_factory.py)

* References: Williams, S. (2019). Retrieved from: https://github.com/faif/python-patterns/blob/master/patterns/creational/abstract_factory.py*

**2. Builder: ** This pattern, by its nature performs the construction of representations by different methods, we use an example when buying a computer that has different characteristics, we take into account the amount of physical memory (RAM), amount of disk space hard drive and graphics card model. The changes are as follows: A builder, director, or hardware engineer is introduced and the construction of the computer is carried out step by step, a basic characteristic of the builder pattern.

[Builder pattern code] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/builder.py)

* References: Kasampalis, S. (2015). Mastering Python Design Patterns. 2nd ed. Birmingham - Bombay: PACKT Publishing, pp. 32 - 33. *

**3. Factory Method: ** The goal is to provide an interface to create families of related objects without specifying the concrete class. We have a simple example of the abstract factory in which we have to decide the name and sex of a person through input methods, the, we insert the name and gender represented by: Male = "M" and Female = "F ". It is done by means of a central class and the factory class is in charge of classifying the functions or instances created by the central abstract class.
We have a class called person with two methods to obtain the name and gender that in turn have two subclasses that allow printing the message. The Factory class has the methods that have the arguments of the person's name and gender. The client instantiates the factory class and calls the methods with the arguments.

[Code the Factory Method pattern] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/factory_method.py)

* References: Verma, R. and Giridhar, C. (2011). Design Patters in Python. 1st ed. Testing Perspective, pp. 33 - 34. *

**4. Simple Factory (Factory of objects in its simplest implementation): ** It is considered a factory because it creates objects of different types instead of instances of direct objects, it is a reference to understand the simplest implementation to visualize the factory method and the factory abstract. We take an example of the implementation of the [factory method] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/factory_method.py) on the selection of animals but this time in the emission of sounds. We create an abstract product called "Animal" from which two products are derived, which are dogs and cats to emit the corresponding sound of the animal. We establish the factory class to call the sounds and pass the type of argument to the client who must choose between dog = "Dog" and cat = "Cat" (sensitive to inputs of type Strings).

[Simple Factory pattern code] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/simple_factory.py)

* References: Giridhar, C. (2016). Learning Python Design Patterns. 2nd ed. Birminghan: PACKT Publishing, pp. 107 - 108. *

**5. Singleton: ** One of the simplest patterns in creational design and guarantees that a class only has one instance and one global access point to it. In the following implementation, we find an example in which instances are created to serve the same object of any type of which we want to design or execute.

[Singleton pattern code] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/singleton.py)

* References: Giridhar, C. (2016). Learning Python Design Patterns. 2nd ed. Birminghan: PACKT Publishing, pp.83 *

** 6. Strategy: ** The main objective, as its name indicates, is to choose based on certain parameters the use of strategies or algorithms that encapsulate them and make them interchangeable between them. In the example taken from the Head Firts book in the first chapter, it explains how a duck can fly in different ways and how the client will be able to choose the strategy to be implemented, which are intercommunicated through interfaces. The subclasses are called through the interfaces that create instances to decide if the duck will fly with its wings, with a "Rocket" or, it will not be able to fly.

[Strategy pattern code] (https://github.com/NapsterZ4/Design-Patterns-in-Python/blob/master/design_patterns/venv/include/design_patterns/strategy.py)

* References: Freeman, E., Robson, E., Sierra, K. and Bates, B. (2009). Head First: Design Patterns. Gravenstein Highway North Sebastopol: OReilly, pp. 50-70. *